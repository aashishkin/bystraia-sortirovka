﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Быстрая_сортировка
{
    class QuickSorting
    {
        public static void sorting(double[] mas, int left, int right)
        {
            double p = mas[(right - left) / 2 + left];
            double temp;
            int i = left, j = right;
            while (i <= j)
            {
                while (mas[i] < p && i <= right)
                {
                    i++;
                }

                while (mas[j] > p && j >= left)
                {
                    j--;
                }

                if (i <= j)
                {
                    temp = mas[i];
                    mas[i] = mas[j];
                    mas[j] = temp;
                    i++; j--;
                }
            }
            if (j > left) sorting(mas, left, j);
            if (i < right) sorting(mas, i, right);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            double[] mas = new double[40];
            //заполняем массив случайными числами
            Random rnd = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(1, 100+1);
            }
            
            Console.WriteLine("До сортировки:");
            foreach (double x in mas)
            {
                Console.Write(x + " ");
            }
            Console.WriteLine();
            //сортировка
            QuickSorting.sorting(mas, 0, mas.Length - 1);
            Console.WriteLine();
            Console.WriteLine("После сортировки:");
            foreach (double x in mas)
            {
                Console.Write(x + " ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Press the <Enter> key");
            Console.ReadLine();
        }
    }
}
